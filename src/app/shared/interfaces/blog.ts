export interface Blog{
    "id": number
    "image":string
    "date": Date
    "title": string
    "description":string
}