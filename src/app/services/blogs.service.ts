import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Blog } from '../shared/interfaces/blog';


@Injectable({
  providedIn: 'root'
})

export class BlogsService {
    private apiUrl = 'http://localhost:3000';

    constructor(
        private _httpClient: HttpClient,
        private router: Router,
      ) {

    }
    
    getBlogs(): Observable<Blog[]> {
       return this._httpClient.get<Blog[]>(`${this.apiUrl}/blogs`)
    }
    }