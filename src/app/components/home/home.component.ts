import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/shared/interfaces/blog';
import { BlogsService } from 'src/app/services/blogs.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  [x: string]: any;

  blogs: Blog[] = [];
  isMenuOpened:boolean=false
  isMenuOpened2:boolean=false
  
  constructor(private blogsService: BlogsService) { 
  }
  
ngOnInit(): void {
  this.blogsService.getBlogs().subscribe(data =>{
  this.blogs = data;
  }) 
}

sortIdAsc(){
  let newarr=this.blogs.sort((a,b)=>a.id-b.id);
}
sortIdDsc(){
  let newarr=this.blogs.sort((a,b)=>b.id-a.id);
}
sortDateAsc(){
  let newdate= this.blogs.sort((a, b) => 
  <any>new Date(a.date) - <any>new Date(b.date));
  }
sortDateDsc(){
    let newdate= this.blogs.sort((a, b) => 
    <any>new Date(b.date) - <any>new Date(a.date));
}
toggleMenu(){
  this.isMenuOpened=!this.isMenuOpened
}
toggleMenu2(){
  this.isMenuOpened2=!this.isMenuOpened2
}
}


// clickOutside(){
//   this.isMenuOpened=false
// }

  // }


  // public sortData() {
  //   return this.blogs.sort((a, b) => {
  //     return <any>new Date(b.date) - <any>new Date(a.date);
  //   });
  // }

  // sortId(){
  //   if(this.isOrdered){
  //     let newarr=this.blogs.sort((a,b)=>a.id-b.id);
  //     this.blogs=newarr
  //   }else{
  //     let newarr=this.blogs.sort((a,b)=>b.id-a.id);
  //     this.blogs=newarr
  //   }
  //   this.isOrdered=!this.isOrdered
  // }